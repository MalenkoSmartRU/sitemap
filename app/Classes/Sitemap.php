<?php

namespace App\Classes;

use DOMDocument;
use App\Classes\Url;
use DOMAttr;
use Curl\Curl;

class Sitemap
{
    // Адрес исследуемого сайта
    protected $url;
    // Имя файла
    protected $file;
    // Относительный путь к файлу (относительно storage/app)
    protected $path;
    
    public function __construct($url, $file, $path)
    {
        $this->url  = new Url($url);
        $this->file = $file; 
        $this->path = $path; 
    }
    
    // Начинает сканирование в соответствии с установками
    public function generate()
    {
        {
            // Получить базовый список ссылок со стартовой страницы
            $baseLinks = $this->scanPage($this->url);
            dump($baseLinks);

            $existingLinks = [];
            foreach ($baseLinks as $link) {
                // Проверить, является ли ссылка абсолютной или относительной
                if (Url::isAbsolute($link)) {

                }
                $curl = new Curl();
                $curl->get($this->url . $link);

                // Если код ответа возвращает статус 'OK', то заносим его в массив
                if ($curl->getInfo()['http_code'] === 200) {
                    $existingLinks[] = $link;
                }
            }
            dd($existingLinks);
        } while (0);

        // Пройтись по каждой странице и получить новый список ссылок
//        foreach ($baseLinks as $link) {
//            $curl = new Curl();
//            $curl->get($this->url->url.'/'.$link);
//            try {
//                if ($curl->error) {
//                    throw new Exception($curl->errorMessage, $curl->errorCode);
//                } else {
//                    var_dump($curl->getInfo()['http_code']);
//                }
//            } catch (Exception $ex) {
//                exit($ex->getMessage() . ' ' . $ex->getCode());
//            }
//        }    
    }
    
    // Сканирует выбранную страницу
    protected function scanPage($url)
    {
        // Отключить ошибки при построении DOM документа
        libxml_use_internal_errors(true);
        // Создать новый документ и загрузить его по ссылке
        $dom = new DOMDocument('1.0', 'UTF-8');
        $dom->loadHTMLFile($url);

        // Получить все теги a на странице
        $linksDOM = $dom->getElementsByTagName('a');
        
        $rawLinks = [];
        for ($i = 0; $i < $linksDOM->length; $i++) {
            $domAttrHref = $linksDOM->item($i)->attributes->getNamedItem('href');
            if ($domAttrHref instanceof DOMAttr) {
                // Получить содержимое аттрибута href всех ссылок
                $rawLinks[] = $domAttrHref->textContent;
            }
        }

        // Обрезать все повторяющиеся значения (оставить только уникальные ссылки)
        $uniqueLinks = array_unique($rawLinks);

        $clearLinks = [];
        // Проверить все значения, являются ли они ссылками внутри текущего адреса
        foreach ($uniqueLinks as $link) {
            $url = new Url($link);
            if (($url->domain === $this->url->domain || $url->domain === null) && !preg_match('{^#}iu', $link)) {
                $clearLinks[] = $url->parameters;
            }
        }
        
        return $clearLinks;
    }
}