<?php
/*
 * Принимает внешний url и преобразует его в соответствии с нужным форматом
 */

namespace App\Classes;

class Url
{
    // Полный url адрес
    protected $url          = null;
    // Протокол адреса
    protected $protocol     = null;
    // Домен, выделенный путем разбора
    protected $domain       = null;
    // Параметры, переданные в url (все, что идет после домена, считается за параметры)
    protected $parameters   = null;
    // Является ли url внешним
    protected $external     = false;
    
    public function __construct($url, $domain = null, $protocol = 'http')
    {
        $this->url = $url;
        $this->parseUrl($url, $domain, $protocol);
    }
    
    // Производит парсинг полученного url и производит заполнение параметров класса полученными значениями
    protected function parseUrl($url, $domain, $protocol)
    {
        // Получить проверяемый домен
        if ($domain === null) {
            // Если домен не установлен
            $currentDomain = filter_input(INPUT_SERVER, 'SERVER_NAME');
        } else {
            // Если домен установлен, нужно его получить
            $domainMatches = [];

            preg_match('{^(?:(?:https?)(?:://)(?<domain>([a-zа-я0-9\-]+\.?)+))?(?:.*)$}iu', $domain, $domainMatches);
            $currentDomain = $domainMatches['domain'];
        }
        
        // Получить url, его параметры и протокол
        $matches = [];
        //                (   протокол  ) (          домен         ) (   доменная зона  )  (параметры)
        
//        $regex1 = '{^(?:(https?)://)?((?:[0-9a-zа-я\-]{1,63}\.?)*(?:[a-zа-я]{2,6}\.?)*)(.*)$}iu';
//        $regex2 = '{^(?:(?<protocol>https?)(?:://)(?<name>([a-zа-я0-9\-]+\.?)+))?(?<parameters>.*)$}iu';
        if (preg_match('{^(?:(?<protocol>https?)(?:://)(?<domain>([a-zа-я0-9\-]+\.?)+))?(?<parameters>.*)$}iu', $url, $matches)) {
            $this->protocol     = $matches['protocol']? $matches['protocol'] : $protocol;
            $this->domain       = $matches['domain']? $matches['domain'] : null;
            $this->parameters   = $matches['parameters'];
        } else {
            $this->protocol     = $protocol;
        }

        // Проверка на внешний или внутренний домен
        if (($this->domain === $currentDomain) || ($this->domain === null)) {
            $this->external = false;
        } else {
            $this->external = true;
        }
    }
    
    // Возвращает запрашиваемые из вне параметры
    public function __get($value)
    {
        switch ($value)
        {
            case 'url':
                return $this->url;
            case 'protocol':
                return $this->protocol;
            case 'domain':
                return $this->domain;
            case 'parameters':
                return $this->parameters;
            case 'external':
                return $this->external;
        }
    }
    
    public function __toString() {
        return $this->url;
    }
    
    // Вернуть true, если путь абсолютный или false, если путь относительный
    public static function isAbsolute($path)
    {
        return preg_match('{^/}iu', $path);
    }
}