<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Classes\Sitemap;

class SitemapBuilder extends Controller
{
    protected $sitemap;
    
    public function __construct()
    {
        $this->sitemap = new Sitemap(config('sitemap.url'), config('sitemap.file'), config('sitemap.path'));
    }
    
    // Инициализация необходимых методов генератора
    protected function init()
    {
        $this->sitemap->generate();
    }
    
    public function show()
    {
        $this->init();
        
        return view('sitemap');
    }
}
